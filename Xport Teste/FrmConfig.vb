﻿Public Class FrmConfig
    Dim config As New Config
    Private Sub FrmConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        config = config.getConfig
        If config Is Nothing Then
            config = New Config
            config.notificacao = 0
            config.tempo = 30
            config.Inserir()
        End If

        If Not config Is Nothing Then
            TxtTempo.Text = config.tempo
            If config.notificacao = 1 Then
                CboxNotification.CheckState = CheckState.Checked

            Else
                CboxNotification.CheckState = CheckState.Unchecked
            End If
        End If
        Dim Chave = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
        Dim Tem = Chave.GetValue("MySQL Teste")
        If Not Tem Is Nothing Then
            CboxAutostart.CheckState = CheckState.Checked
        Else
            CboxAutostart.CheckState = CheckState.Unchecked
        End If


    End Sub

    Private Sub BtnCancelar_Click(sender As Object, e As EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub


    Private Sub BtnSalvar_Click(sender As Object, e As EventArgs) Handles BtnSalvar.Click
        If CboxNotification.CheckState = CheckState.Checked Then
            config.notificacao = "1"
        Else
            config.notificacao = "0"
        End If
        config.tempo = TxtTempo.Text
        config.Alterar()
        MessageBox.Show("Configurações salvas com sucesso!")
        Me.Close()
    End Sub

    Private Sub CboxAutostart_CheckedChanged(sender As Object, e As EventArgs) Handles CboxAutostart.CheckedChanged
        Dim exePath As String = Application.ExecutablePath

        If CboxAutostart.CheckState = CheckState.Checked Then
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "MySQL Teste", exePath)

        Else
            Dim Chave = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
            Chave.DeleteValue("MySQL Teste")
        End If
    End Sub
End Class