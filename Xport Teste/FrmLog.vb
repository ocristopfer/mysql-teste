﻿Imports System.Net.Mime.MediaTypeNames
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office
Imports Microsoft.Office.Interop
Imports DevExpress.XtraGrid.Views.Grid

Public Class FrmLog
    Dim _Log As New List(Of Log)
    Dim Log As New Log
    Dim Unidades As New Unidades
    Dim _Unidades As New List(Of Unidades)
    Dim online As Long = 0
    Dim offline As Long = 0




    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BtnFiltrar.Click
        _Log = Nothing
        DgvLog.DataSource = Nothing
        Dim dti As Date = Nothing
        Dim dtf As Date = Nothing
        Dim unit As String = "TODOS"

        If DtpInicial.Checked = True And DtpFinal.Checked = True Then
            dti = DtpInicial.Value
            dtf = DtpFinal.Value
        ElseIf DtpInicial.Checked = True And DtpFinal.Checked = False Then
            dti = DtpInicial.Value
            dtf = Nothing
        ElseIf DtpInicial.Checked = False And DtpFinal.Checked = True Then
            dti = Nothing
            dtf = DtpFinal.Value
        Else
            dti = Nothing
            dtf = Nothing
        End If

        '' Dim sql As New System.Text.StringBuilder
        ''  With sql
        'For Each oRow As DataRow In oDt.Rows
        '    If String.IsNullOrEmpty(Sql.ToString) Then
        '        .AppendLine("( ").AppendLine(" " & oRow("teste"))
        '    Else
        '        .AppendLine(" or " & oRow("teste"))
        '    End If
        'Next
        '.AppendLine(" ) ")

        'End With



        If ListUnidades.SelectedItems.Count > 1 Then
            Dim sql As New System.Text.StringBuilder
            With sql
                For Each i As Unidades In ListUnidades.SelectedItems
                    If String.IsNullOrEmpty(sql.ToString) Then
                        .AppendLine("and ( ").AppendLine("l.unidade =  '" & i.Unidade & "'")
                    Else
                        sql.AppendLine("or l.unidade = '" & i.Unidade & "' ")
                    End If

                Next
                sql.AppendLine(")")
            End With
            unit = sql.ToString

        Else

            If ListUnidades.SelectedValue.ToString <> "TODOS" Then
                unit = "and l.unidade = '" + ListUnidades.SelectedValue.ToString + "'"
            End If

            ' If CmboxUnidades.SelectedValue.ToString() <> "TODOS" Then
            'unit = "and l.unidade = '" + CmboxUnidades.SelectedValue.ToString + "'"
            ' End If


        End If

        _Log = Log.getLogs(dti, dtf, unit)
        ' DgvLogs.DataSource = _Log
        DgvLog.DataSource = _Log

        online = 0
        offline = 0

        For i As Integer = 0 To DgvLogs.DataRowCount - 1
            If DgvLogs.GetRowCellValue(i, "status").ToString = "Está Online" Then

                online = online + 1

            ElseIf DgvLogs.GetRowCellValue(i, "status").ToString = "Está Offline" Then

                offline = offline + 1

            End If
        Next
        LblOnline.Text = "Total Online: " & online.ToString
        LblOffline.Text = "Total Offline: " & offline.ToString
        '  LblOffline.Text = LblOffline.Text & DgvLog.DataMember(3)


        ''   DgvLog.DataBindings = _Log
        'For Each Rowns As DataGridViewRow In DgvLogs.Rows
        '    If Rowns.Cells(2).Value.ToString = "Está Online" Then
        '        Rowns.DefaultCellStyle.ForeColor = Color.Green
        '    ElseIf Rowns.Cells(2).Value.ToString = "Está Offline" Then
        '        Rowns.DefaultCellStyle.ForeColor = Color.Red
        '    End If
        'Next



    End Sub



    Private Sub FrmLog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '_Log = Log.getLogs
        _Log = Nothing
        DgvLog.DataSource = Nothing
        'DgvLog.AutoGenerateColumns = False

        _Unidades = Unidades.getUnidades(True)
        ListUnidades.DataSource = Unidades.getUnidades(True)


        ListUnidades.ValueMember = "Unidade"
        ListUnidades.DisplayMember = "Unidade"


        CmboxUnidades.DataSource = _Unidades
        CmboxUnidades.DisplayMember = "Unidade"
        CmboxUnidades.ValueMember = "Unidade"
        LblUnidades.Text = "Unidades: " & ListUnidades.Items.Count - 1

        online = 0
        offline = 0
        LblOnline.Text = "Total Online: " & online.ToString
        LblOffline.Text = "Total Offline: " & offline.ToString

        ' ListBox1.DataSource = _Unidades
        ' ListBox1.DisplayMember = "Unidade"
        ' ListBox1.ValueMember = "Unidade"







    End Sub


    Private Sub BtnExecel_Click(sender As Object, e As EventArgs) Handles BtnExecel.Click
        Dim FileDialog As New SaveFileDialog
        FileDialog.Filter = "Planilha | *.xls"
        FileDialog.DefaultExt = "xls"
        FileDialog.ShowDialog()


        Dim optionDev As New DevExpress.XtraPrinting.XlsxExportOptions

        optionDev.TextExportMode = DevExpress.XtraPrinting.TextExportMode.Text

        If FileDialog.FileName = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show("Selecione um caminho para salvar o aquivos .xls", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            FileDialog.ShowDialog()
            ' GridControl1.ExportToXlsx("planilha.xls", optionDev)
            ' Process.Start("planilha.xls")
        Else

            DgvLogs.ExportToXlsx(FileDialog.FileName, optionDev)
            Process.Start(FileDialog.FileName)
        End If

    End Sub

    Private Sub GridView1_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles DgvLogs.RowStyle
        Dim View As GridView = sender
        If View.GetRowCellValue(e.RowHandle, "status") = "Está Online" Then
            e.Appearance.ForeColor = Color.Green
        ElseIf View.GetRowCellValue(e.RowHandle, "status") = "Está Offline" Then
            e.Appearance.ForeColor = Color.Red
        End If

    End Sub


    Private Sub FrmLog_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave

    End Sub

    Private Sub CheckBoxAtivos_CheckStateChanged(sender As Object, e As EventArgs) Handles CheckBoxAtivos.CheckStateChanged
        If CheckBoxAtivos.Checked = True Then
            _Unidades = Unidades.getUnidades(True)
            CmboxUnidades.DataSource = _Unidades
            ListUnidades.DataSource = Unidades.getUnidades(True)
        Else
            _Unidades = Unidades.getUnidades(True, 0)
            CmboxUnidades.DataSource = _Unidades
            ListUnidades.DataSource = Unidades.getUnidades(True, 0)
        End If
        ListUnidades.Update()
        CmboxUnidades.Update()
        ListUnidades.Refresh()
        CmboxUnidades.Refresh()
    End Sub


End Class