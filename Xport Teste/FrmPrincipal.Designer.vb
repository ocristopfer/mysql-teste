﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPrincipal
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPrincipal))
        Me.Lista = New System.Windows.Forms.DataGridView()
        Me.Unidade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Temporizador = New System.Windows.Forms.Timer(Me.components)
        Me.LblProxTeste = New System.Windows.Forms.Label()
        Me.LblTime = New System.Windows.Forms.Label()
        Me.Notificacao = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.MenuNotificação = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AbirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CadastrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfigToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Painel = New System.Windows.Forms.Panel()
        Me.ProgressBarControlTeste = New DevExpress.XtraEditors.ProgressBarControl()
        Me.BtnTestar = New DevExpress.XtraEditors.SimpleButton()
        Me.LblTestando = New System.Windows.Forms.Label()
        Me.BtnFechar = New System.Windows.Forms.Button()
        Me.LblTitulo = New System.Windows.Forms.Label()
        Me.DireitosAutorais = New System.Windows.Forms.Label()
        CType(Me.Lista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuNotificação.SuspendLayout()
        Me.Painel.SuspendLayout()
        CType(Me.ProgressBarControlTeste.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Lista
        '
        Me.Lista.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Lista.AllowUserToAddRows = False
        Me.Lista.AllowUserToDeleteRows = False
        Me.Lista.AllowUserToResizeColumns = False
        Me.Lista.AllowUserToResizeRows = False
        Me.Lista.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Lista.BackgroundColor = System.Drawing.SystemColors.Window
        Me.Lista.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Lista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Lista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Unidade, Me.Status})
        Me.Lista.Cursor = System.Windows.Forms.Cursors.Default
        Me.Lista.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Lista.Location = New System.Drawing.Point(9, 6)
        Me.Lista.MultiSelect = False
        Me.Lista.Name = "Lista"
        Me.Lista.ReadOnly = True
        Me.Lista.RowHeadersVisible = False
        Me.Lista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.Lista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.Lista.Size = New System.Drawing.Size(241, 276)
        Me.Lista.TabIndex = 1
        '
        'Unidade
        '
        Me.Unidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Unidade.DataPropertyName = "Unidade"
        Me.Unidade.HeaderText = "Unidade"
        Me.Unidade.Name = "Unidade"
        Me.Unidade.ReadOnly = True
        Me.Unidade.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'Status
        '
        Me.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'Temporizador
        '
        '
        'LblProxTeste
        '
        Me.LblProxTeste.AutoSize = True
        Me.LblProxTeste.BackColor = System.Drawing.SystemColors.Window
        Me.LblProxTeste.Location = New System.Drawing.Point(113, 339)
        Me.LblProxTeste.Name = "LblProxTeste"
        Me.LblProxTeste.Size = New System.Drawing.Size(77, 13)
        Me.LblProxTeste.TabIndex = 6
        Me.LblProxTeste.Text = "Próximo Teste:"
        '
        'LblTime
        '
        Me.LblTime.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblTime.AutoSize = True
        Me.LblTime.BackColor = System.Drawing.SystemColors.Window
        Me.LblTime.Location = New System.Drawing.Point(189, 339)
        Me.LblTime.Name = "LblTime"
        Me.LblTime.Size = New System.Drawing.Size(40, 13)
        Me.LblTime.TabIndex = 7
        Me.LblTime.Text = "Tempo"
        '
        'Notificacao
        '
        Me.Notificacao.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.Notificacao.ContextMenuStrip = Me.MenuNotificação
        Me.Notificacao.Icon = CType(resources.GetObject("Notificacao.Icon"), System.Drawing.Icon)
        Me.Notificacao.Text = "MySQL Teste"
        Me.Notificacao.Visible = True
        '
        'MenuNotificação
        '
        Me.MenuNotificação.BackColor = System.Drawing.SystemColors.Window
        Me.MenuNotificação.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbirToolStripMenuItem, Me.CadastrarToolStripMenuItem, Me.LogsToolStripMenuItem, Me.ConfigToolStripMenuItem1, Me.SairToolStripMenuItem})
        Me.MenuNotificação.Name = "MenuNotificação"
        Me.MenuNotificação.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuNotificação.ShowImageMargin = False
        Me.MenuNotificação.Size = New System.Drawing.Size(100, 114)
        '
        'AbirToolStripMenuItem
        '
        Me.AbirToolStripMenuItem.Name = "AbirToolStripMenuItem"
        Me.AbirToolStripMenuItem.Size = New System.Drawing.Size(99, 22)
        Me.AbirToolStripMenuItem.Text = "Abrir"
        '
        'CadastrarToolStripMenuItem
        '
        Me.CadastrarToolStripMenuItem.Name = "CadastrarToolStripMenuItem"
        Me.CadastrarToolStripMenuItem.Size = New System.Drawing.Size(99, 22)
        Me.CadastrarToolStripMenuItem.Text = "Cadastrar"
        '
        'LogsToolStripMenuItem
        '
        Me.LogsToolStripMenuItem.Name = "LogsToolStripMenuItem"
        Me.LogsToolStripMenuItem.Size = New System.Drawing.Size(99, 22)
        Me.LogsToolStripMenuItem.Text = "Logs"
        '
        'ConfigToolStripMenuItem1
        '
        Me.ConfigToolStripMenuItem1.Name = "ConfigToolStripMenuItem1"
        Me.ConfigToolStripMenuItem1.Size = New System.Drawing.Size(99, 22)
        Me.ConfigToolStripMenuItem1.Text = "Config"
        '
        'SairToolStripMenuItem
        '
        Me.SairToolStripMenuItem.Name = "SairToolStripMenuItem"
        Me.SairToolStripMenuItem.Size = New System.Drawing.Size(99, 22)
        Me.SairToolStripMenuItem.Text = "Sair"
        '
        'Painel
        '
        Me.Painel.BackColor = System.Drawing.SystemColors.Window
        Me.Painel.Controls.Add(Me.ProgressBarControlTeste)
        Me.Painel.Controls.Add(Me.BtnTestar)
        Me.Painel.Controls.Add(Me.LblTestando)
        Me.Painel.Controls.Add(Me.LblTime)
        Me.Painel.Controls.Add(Me.LblProxTeste)
        Me.Painel.Controls.Add(Me.Lista)
        Me.Painel.Location = New System.Drawing.Point(7, 24)
        Me.Painel.Name = "Painel"
        Me.Painel.Size = New System.Drawing.Size(260, 360)
        Me.Painel.TabIndex = 9
        '
        'ProgressBarControlTeste
        '
        Me.ProgressBarControlTeste.Location = New System.Drawing.Point(9, 304)
        Me.ProgressBarControlTeste.Name = "ProgressBarControlTeste"
        Me.ProgressBarControlTeste.Size = New System.Drawing.Size(241, 24)
        Me.ProgressBarControlTeste.TabIndex = 16
        '
        'BtnTestar
        '
        Me.BtnTestar.Image = CType(resources.GetObject("BtnTestar.Image"), System.Drawing.Image)
        Me.BtnTestar.Location = New System.Drawing.Point(9, 334)
        Me.BtnTestar.Name = "BtnTestar"
        Me.BtnTestar.Size = New System.Drawing.Size(84, 23)
        Me.BtnTestar.TabIndex = 15
        Me.BtnTestar.Text = "Testar"
        '
        'LblTestando
        '
        Me.LblTestando.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.LblTestando.AutoSize = True
        Me.LblTestando.BackColor = System.Drawing.SystemColors.Window
        Me.LblTestando.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTestando.Location = New System.Drawing.Point(69, 285)
        Me.LblTestando.Name = "LblTestando"
        Me.LblTestando.Size = New System.Drawing.Size(121, 16)
        Me.LblTestando.TabIndex = 14
        Me.LblTestando.Text = "Aguardando Teste"
        Me.LblTestando.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'BtnFechar
        '
        Me.BtnFechar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnFechar.AutoSize = True
        Me.BtnFechar.Location = New System.Drawing.Point(240, 1)
        Me.BtnFechar.Name = "BtnFechar"
        Me.BtnFechar.Size = New System.Drawing.Size(25, 23)
        Me.BtnFechar.TabIndex = 8
        Me.BtnFechar.Text = "X"
        Me.BtnFechar.UseVisualStyleBackColor = True
        '
        'LblTitulo
        '
        Me.LblTitulo.AutoSize = True
        Me.LblTitulo.Location = New System.Drawing.Point(9, 6)
        Me.LblTitulo.Name = "LblTitulo"
        Me.LblTitulo.Size = New System.Drawing.Size(0, 13)
        Me.LblTitulo.TabIndex = 10
        '
        'DireitosAutorais
        '
        Me.DireitosAutorais.AutoSize = True
        Me.DireitosAutorais.Location = New System.Drawing.Point(8, 387)
        Me.DireitosAutorais.Name = "DireitosAutorais"
        Me.DireitosAutorais.Size = New System.Drawing.Size(171, 13)
        Me.DireitosAutorais.TabIndex = 11
        Me.DireitosAutorais.Text = "Copyright © 2018 - Cristopfer Luis ."
        '
        'FrmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(274, 409)
        Me.ContextMenuStrip = Me.MenuNotificação
        Me.Controls.Add(Me.DireitosAutorais)
        Me.Controls.Add(Me.LblTitulo)
        Me.Controls.Add(Me.BtnFechar)
        Me.Controls.Add(Me.Painel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "MySQL Teste"
        CType(Me.Lista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuNotificação.ResumeLayout(False)
        Me.Painel.ResumeLayout(False)
        Me.Painel.PerformLayout()
        CType(Me.ProgressBarControlTeste.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lista As DataGridView
    Friend WithEvents Temporizador As Timer
    Friend WithEvents LblProxTeste As Label
    Friend WithEvents LblTime As Label
    Friend WithEvents Notificacao As NotifyIcon
    Friend WithEvents Painel As Panel
    Friend WithEvents BtnFechar As Button
    Friend WithEvents LblTitulo As Label
    Friend WithEvents DireitosAutorais As Label
    Friend WithEvents MenuNotificação As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AbirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CadastrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfigToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Unidade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblTestando As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnTestar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ProgressBarControlTeste As DevExpress.XtraEditors.ProgressBarControl
End Class
