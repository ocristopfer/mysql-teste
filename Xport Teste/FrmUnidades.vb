﻿Public Enum Estado
    Inclusao = 1
    Edicao = 2
    Pesquisa = 3
End Enum

Public Class FrmUnidades
    Dim Unidade As New Unidades
    Dim Unidades As New List(Of Unidades)
    Private Sub Grid()
        'If Unidades.Count = 0 Then
        Unidades = Unidade.getUnidades(, 0)
        'End If
        DgvUnidades.DataSource = Unidades
        DgvUnidades.Refresh()
    End Sub
    Private Sub Config_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DgvUnidades.AutoGenerateColumns = False
        Grid()
    End Sub
    Public Function getUnidade() As Unidades
        Return Unidade
    End Function


    Private Sub PreencherTela()
        txtUnidade.Text = Unidade.Unidade
        txtLink.Text = Unidade.Link
        txtUser.Text = Unidade.User
        txtPass.Text = Conexao.Cript(Unidade.Pass)
        txtPorta.Text = Unidade.Porta
        CboxAtivo.CheckState = Unidade.ativo
    End Sub
    Private Sub GerenciarEstado(ByVal pEstado As Estado)
        Select Case pEstado
            Case Estado.Inclusao
                Me.BtnExcluir.Enabled = False
                Me.btnSalvar.Enabled = True
                Me.btnSalvar.Text = "Salvar"
                Me.btnCancelar.Enabled = True
                Me.btnNovo.Enabled = False
                Me.PnDados.Enabled = True

            Case Estado.Pesquisa
                Me.BtnExcluir.Enabled = False
                Me.btnSalvar.Enabled = False
                Me.btnSalvar.Text = "Salvar"
                Me.btnCancelar.Enabled = False
                Me.btnNovo.Enabled = True
                Me.PnDados.Enabled = False
            Case Estado.Edicao
                Me.BtnExcluir.Enabled = True
                Me.btnSalvar.Enabled = True
                Me.btnSalvar.Text = "Alterar"
                Me.btnCancelar.Enabled = True
                Me.btnNovo.Enabled = False
                Me.PnDados.Enabled = True
        End Select
    End Sub


    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        LimparCampos()
        Unidade = Nothing
        GerenciarEstado(Estado.Inclusao)
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs)
        If Unidade Is Nothing Then
            MessageBox.Show("Selecione uma unidade antes de editar!", "Atenção")
        Else
            GerenciarEstado(Estado.Edicao)
        End If
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        Salvar()
        Grid()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        'Me.Close()
        GerenciarEstado(Estado.Pesquisa)
        Grid()
    End Sub

    Private Sub PreencherObjetos()
        If Me.txtPass.Text.Trim = "" Or Me.txtUser.Text.Trim = "" Then
            Unidade = Nothing
            Throw New Exception("Por favor informe o usuario e senha antes de finalizar o cadastramento.")
        End If
        Unidade.Unidade = txtUnidade.Text
        Unidade.Link = txtLink.Text
        Unidade.User = txtUser.Text
        Unidade.Pass = Conexao.Cript(txtPass.Text)
        Unidade.Porta = txtPorta.Text
        Unidade.ativo = CboxAtivo.CheckState
    End Sub

    Private Sub LimparCampos()
        txtUnidade.Text = Nothing
        txtLink.Text = Nothing
        txtUser.Text = Nothing
        txtPass.Text = Nothing
        txtPorta.Text = Nothing
        CboxAtivo.Checked = False
    End Sub
    Private Sub Salvar()
        Try
            If Unidade Is Nothing Then
                Unidade = New Unidades
                PreencherObjetos()
                If Unidade.Salvar() Then
                    MessageBox.Show("Consultor cadastrado com sucesso!", "Atenção")
                    LimparCampos()
                    Grid()
                    GerenciarEstado(Estado.Pesquisa)
                End If
            Else
                PreencherObjetos()
                If Unidade.Alterar() Then
                    MessageBox.Show("Consultor alterado com sucesso!", "Atenção")
                    LimparCampos()
                    Grid()
                    GerenciarEstado(Estado.Pesquisa)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Atenção")
        End Try

    End Sub

    Private Sub DgvUnidades_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvUnidades.CellDoubleClick
        If e.RowIndex >= 0 Then
            Unidade = Me.DgvUnidades.Rows(e.RowIndex).DataBoundItem
            GerenciarEstado(Estado.Edicao)
            Unidade = getUnidade()
            PreencherTela()
        End If
    End Sub

    Private Sub BtnExcluir_Click(sender As Object, e As EventArgs) Handles BtnExcluir.Click
        Dim rows = DgvUnidades.SelectedCells(0).Value.ToString

        Dim Box = MessageBox.Show("Tem certeza que deseja excluir " & rows, "", MessageBoxButtons.YesNo)

        If Box = Windows.Forms.DialogResult.Yes Then
            Unidade.Deletar()
            MessageBox.Show(rows & "Excluido com sucesso!", "Atenção!", MessageBoxButtons.OK)
            LimparCampos()
        Else

        End If
        Grid()


    End Sub


End Class