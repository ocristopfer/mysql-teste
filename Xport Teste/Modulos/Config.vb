﻿<MU.PersistenceFramework.PersistenceLayer.Attributes.attTableName("config")> _
Public Class Config
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attKeyColumnAutonum()> _
    Public Property id As Long
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.smallint)> _
    Public Property notificacao As Integer
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.smallint)> _
    Public Property tempo As Integer

    Public Function getConfig()
        Dim Sql As New System.Text.StringBuilder

        With Sql
            .AppendLine("select * from config")
        End With

        Return Conexao.getDataManagerWeb.loadObject(Sql.ToString, GetType(Config))
    End Function

    Public Function Alterar() As Boolean
        Return Conexao.getDataManagerWeb.UpdateData(Me)
    End Function

    Public Function Inserir() As Boolean
        Return Conexao.getDataManagerWeb.InsertData(Me)
    End Function

End Class
