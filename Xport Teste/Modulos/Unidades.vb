﻿<MU.PersistenceFramework.PersistenceLayer.Attributes.attTableName("unidades")> _
Public Class Unidades
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attKeyColumnAutonum()> _
    Public Property id As Long
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 100)> _
    Public Property Unidade As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 100)> _
    Public Property Link As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 30)> _
    Public Property User As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 30)> _
    Public Property Pass As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 5)> _
    Public Property Porta As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.smallint)> _
    Public Property ativo As Integer

    Public Shared Function getTableName()
        Return "unidades"
    End Function

    Public Function getUnidades(Optional ByVal Unidades As Boolean = False, Optional ByVal Ativo As Integer = 1) As List(Of Unidades)
        Dim Sql As New System.Text.StringBuilder

        With Sql
            If Unidades = True Then
                .AppendLine("select  'TODOS' as Unidade")
                .AppendLine("union")
                .AppendLine("select Unidade from " & getTableName())
                If Ativo = 1 Then
                    .AppendLine("where ativo = @ativo")
                End If

            Else
                .AppendLine("select * from " & getTableName())
                If Ativo = 1 Then
                    .AppendLine("where ativo = @ativo")
                End If

            End If

        End With

        Dim cmd As MySql.Data.MySqlClient.MySqlCommand = Conexao.getConnection.CreateCommand
        cmd.Parameters.AddWithValue("@ativo", Ativo)
        cmd.CommandText = Sql.ToString

        Return Conexao.getDataManagerWeb.getObjects(Of Unidades)(cmd)

    End Function

    Public Shared Function getSolicitante(ByVal id As Long) As Unidades
        Return Conexao.getDataManagerWeb.loadObject(id, GetType(Unidades))
    End Function

    Public Function Salvar() As Boolean
        Return Conexao.getDataManagerWeb.InsertData(Me)
    End Function

    Public Function Alterar() As Boolean
        Return Conexao.getDataManagerWeb.UpdateData(Me)
    End Function

    Public Function Deletar() As Boolean
        Return Conexao.getDataManagerWeb.DeleteData(Me)
    End Function



    Public Function Lista() As List(Of Unidades)
        Dim _Unidades As New List(Of Unidades) From
            {
                New Unidades With {.Unidade = "001 Pr1mera", .Link = "xport-itanhanga.ddns.net"},
                New Unidades With {.Unidade = "002 Xports Rocinha", .Link = "xport-rocinha.ddns.net"},
                New Unidades With {.Unidade = "003 Xports Rio das Pedras", .Link = "xport-riopedras.ddns.net"},
                New Unidades With {.Unidade = "004 Xports Gardenia", .Link = "xport-gardenia.ddns.net"},
                New Unidades With {.Unidade = "005 Workout Buzios", .Link = "xport-buzios.ddns.net"},
                New Unidades With {.Unidade = "006 Xports Rio Comprido", .Link = "xport-riocomprido.ddns.net"},
                New Unidades With {.Unidade = "001 Giga Gym", .Link = "gigagym.dlinkddns.com"},
                New Unidades With {.Unidade = "001 Podium Academia", .Link = "podium.dlinkddns.com"}
            }
        Return _Unidades
    End Function
End Class
