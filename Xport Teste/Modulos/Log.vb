﻿<MU.PersistenceFramework.PersistenceLayer.Attributes.attTableName("log")> _
Public Class Log
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attKeyColumnAutonum()> _
    Public Property id As Long
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.int)> _
    Public Property fkunidade As Long
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 100)> _
    Public Property unidade As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.datetime)> _
    Public Property dateteste As Date
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 50)> _
    Public Property status As String
    <MU.PersistenceFramework.PersistenceLayer.Attributes.attColumn(MU.PersistenceFramework.PersistenceLayer.Attributes.eSqlDataType.varchar, 255)> _
    Public Property erro As String

    Public Shared Function getTableName()
        Return "log"
    End Function
    Public Function getLogs(Optional ByVal DtInicial As Date = Nothing, Optional ByVal DtFinal As Date = Nothing, Optional ByVal Unidade As String = "TODOS") As List(Of Log)
        Dim Sql As New System.Text.StringBuilder

        With Sql
            .AppendLine("select l.unidade,l.dateteste,l.`status`,l.erro from " & getTableName() & " as l")
            .AppendLine("inner join " & Unidades.getTableName & " u")
            .AppendLine("on u.id = l.fkunidade")
            .AppendLine("where 1=1")
            .AppendLine("and u.ativo = 1")


            If Not DtInicial = Nothing Then
                .AppendLine("and date(l.dateteste) >= date(@dti)")
            End If

            If Not DtFinal = Nothing Then
                .AppendLine("and date(l.dateteste) <= date(@dtf)")
            End If

            If Unidade <> "TODOS" Then
                ' .AppendLine("and l.unidade =")
                .AppendLine(Unidade)
            End If

        End With


        'Return Conexao.getDataManagerWeb.getObjects(Of Unidades)(cmd)

        Dim cmd As MySql.Data.MySqlClient.MySqlCommand = Conexao.getConnection.CreateCommand
        cmd.CommandText = Sql.ToString
        If Not DtInicial = Nothing Then
            cmd.Parameters.AddWithValue("@dti", DtInicial)
        End If
        If Not DtFinal = Nothing Then
            cmd.Parameters.AddWithValue("@dtf", DtFinal)
        End If

        Return Conexao.getDataManagerWeb.getObjects(Of Log)(cmd)

    End Function

    Public Function Alterar() As Boolean
        Return Conexao.getDataManagerWeb.UpdateData(Me)
    End Function

    Public Function Inserir() As Boolean
        Return Conexao.getDataManagerWeb.InsertData(Me)
    End Function
End Class
