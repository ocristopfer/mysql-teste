﻿Public Class Conexao

    Private Shared oSqlConnection As SqlClient.SqlConnection
    Private Shared cnn As MySql.Data.MySqlClient.MySqlConnection
    Private Shared _connection As MySql.Data.MySqlClient.MySqlConnection
    Private Shared _DataManager As MU.PersistenceFramework.PersistenceLayer.DataManager


    Public Shared Function getDataManagerWeb() As MU.PersistenceFramework.PersistenceLayer.DataManager
        If _DataManager Is Nothing Then

            _DataManager = New MU.PersistenceFramework.PersistenceLayer.DataManager(getConnection)


        End If
        Return _DataManager
    End Function

    Public Shared Function createDatabase(ByVal conection As Data.Common.DbConnection) As Boolean

        Dim oMySqlConnection As New MySql.Data.MySqlClient.MySqlConnection
        Dim retorno As Boolean
        Dim ocommand As MySql.Data.MySqlClient.MySqlCommand = conection.CreateCommand

        If Not ocommand.Connection.State = ConnectionState.Open Then
            ocommand.Connection.Open()
        End If

        ocommand.CommandText = String.Format("CREATE DATABASE IF NOT EXISTS {0}", "mysqlcon")

        retorno = ocommand.ExecuteNonQuery()

        If ocommand.Connection.State = ConnectionState.Open Then
            ocommand.Connection.Close()
        End If

        Return retorno
    End Function



    Public Shared Function Cript(ByVal strSenha As String) As String
        Dim nLetra As String
        Dim cSenha As String = Nothing
        Dim i As Integer

        Cript = ""

        strSenha = strSenha

        For i = 1 To Len(strSenha)
            nLetra = Asc(Mid(strSenha, i, 1))
            nLetra = Chr(Trim(Str(Val(nLetra) Xor (i * 2))))
            cSenha = cSenha & nLetra
        Next
        Cript = cSenha
    End Function


    Public Shared Function getConnection() As MySql.Data.MySqlClient.MySqlConnection
        If cnn Is Nothing Then
            Dim cnnString As String = "server=127.0.0.1;Port=3952;database=mysqlcon;Uid=root;Pwd=suporte"

            'Dim cnnString As String
            'Dim server As String = get_fitness_registry("ServerName")
            'Dim database As String = "mysqlcon"
            'Dim psw As String = Cript(get_fitness_registry("Password"))
            'Dim user As String = get_fitness_registry("Username")
            'Dim remotePort As String = get_fitness_registry("RemotePort")
            'cnnString = "server=" & server & ";Port=" & remotePort & ";database=" & database & ";Uid=" & user & ";Pwd=" & psw
            '
            cnn = New MySql.Data.MySqlClient.MySqlConnection(cnnString)
            Try
                cnn.Open()
                cnn.Close()
            Catch ex As Exception
                cnnString = "server=127.0.0.1;Port=3952;Uid=root;Pwd=suporte"
                'cnnString = "server=" & server & ";Port=" & remotePort & ";Uid=" & user & ";Pwd=" & psw
                cnn = New MySql.Data.MySqlClient.MySqlConnection(cnnString)
                createDatabase(cnn)
                cnn.Open()
                cnn.Close()
                cnnString = "server=127.0.0.1;Port=3952;database=mysqlcon;Uid=root;Pwd=suporte"
                'cnnString = "server=" & server & ";Port=" & remotePort & ";database=" & database & ";Uid=" & user & ";Pwd=" & psw
                cnn = New MySql.Data.MySqlClient.MySqlConnection(cnnString)
            End Try


        End If



        Return cnn
    End Function

    Private Shared Function get_fitness_registry(ByVal nome As String) As String
        Dim regVersion As Microsoft.Win32.RegistryKey
        regVersion = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Micro University 2\\Micro Fitness Mysql")
        Return regVersion.GetValue(nome)
    End Function
End Class
