﻿Imports MySql.Data.MySqlClient
Imports System.Timers
Imports System.Threading
Imports System.Security
Imports System.IO
Imports System.Text
Imports Microsoft.Win32
Imports System.IO.DirectoryInfo

Public Class FrmPrincipal
#Region "Váriaveis"
    Dim min As Integer
    Dim Unidades As New Unidades
    Dim _Unidades As New List(Of Unidades)
    'Dim _Link = Unidades.Lista
    Dim Config As New Config
    Dim Log As New Log
    Dim _Log As New List(Of Log)
    Dim pid As Long
    Dim Tentou As Integer = 0
#End Region
    Private Sub ExecutarMySQL(Optional ByVal Tentativas As Integer = 0)
        Try
            Dim Chave = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\MySQL Teste").GetValue("MySQL Pid").ToString
            Try
                If Chave <> "" Then
                    Dim aProcess As New System.Diagnostics.Process
                    aProcess = System.Diagnostics.Process.GetProcessById(Chave.ToString)
                    aProcess.Kill()
                End If
            Catch ex As Exception

            End Try


            Dim p As New Process()
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            p.StartInfo.FileName = Application.StartupPath + "\MySQL Server\bin\mysqld"
            p.Start()
            pid = p.Id
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\MySQL Teste", "MySQL Pid", pid)

        Catch
            If Tentativas < 3 Then
                Tentou = Tentou + 1
                ExecutarMySQL(Tentou)
            Else
                MessageBox.Show("Erro ao tentar iniciar serviço MySQL, verifique se está tudo correto com a pasta do sistema." & vbNewLine & "Aplicação será encerrada!")
                My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\MySQL Teste", "MySQL Pid", "")
                Environment.Exit(0)
            End If


        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ExecutarMySQL()

        _Unidades = Unidades.getUnidades
        Config = Config.getConfig
        _Log = Log.getLogs

        LblTitulo.Text = "MySQL Teste " + Me.GetType.Assembly.GetName.Version.ToString
        If Config Is Nothing Then
            Config = New Config
            Config.notificacao = 0
            Config.tempo = 30
            Config.Inserir()
            min = Config.tempo
        Else
            min = Config.tempo
        End If


        Temporizador.Interval = 60000
        Temporizador.Enabled = True
        Lista.AutoGenerateColumns = False
        If _Unidades.Count = 0 Then
            MessageBox.Show("Necessário o cadastramento de um link para teste.")
        Else
            Lista.DataSource = _Unidades
        End If

        LblTime.Text = min.ToString + " Minuto(s)"
        setDefaultLocation()

    End Sub

#Region "Lauyout"
    Private Sub setDefaultLocation()
        Dim tamanho = Screen.PrimaryScreen.WorkingArea.Size
        Dim yFinal = tamanho.Height - Me.Height - 2
        Dim xFinal = tamanho.Width - Me.Width
        Me.Location = New Point(xFinal, yFinal)
    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        'Me.Lista.Refresh()
        If Me.WindowState = FormWindowState.Minimized Then
            If Config.notificacao = 1 Then
                Notificacao.Visible = True
                Notificacao.BalloonTipIcon = ToolTipIcon.Info
                Notificacao.BalloonTipTitle = "MySQL Teste"
                Notificacao.BalloonTipText = "MySQL Teste foi minizadado para area de notificação."
                Notificacao.ShowBalloonTip(50000)
            End If
            ShowInTaskbar = False
        End If
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles Notificacao.MouseDoubleClick
        If Me.WindowState = FormWindowState.Normal Then
            Me.WindowState = FormWindowState.Minimized
            ShowInTaskbar = False
        Else
            Me.WindowState = FormWindowState.Normal
            ShowInTaskbar = True
        End If

    End Sub
#End Region
    Private Sub NewLog(Optional ByVal erro As String = "")
        Log.fkunidade = Unidades.id
        Log.unidade = Unidades.Unidade
        Log.dateteste = Date.Now
        Log.erro = erro
        'Log.status = "Está Online"
        Log.Inserir()

    End Sub
    Private Sub MySQL_Teste(Unidades As Unidades)
        Dim conn As New MySqlConnection
        Dim status As String

        conn.ConnectionString = String.Format("server={0}; user={1}; port={2} ; password={3};Connect Timeout=30", Unidades.Link, Unidades.User, Unidades.Porta, Conexao.Cript(Unidades.Pass))

        Dim Sql = conn.CreateCommand()
        'conn.ConnectionTimeout = 30
        Sql.CommandTimeout = 300
        Sql.CommandText = "flush hosts;"
        Try

            conn.Open()
            Sql.ExecuteNonQuery()
            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Value = "Online"
            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Style.ForeColor = Color.Green

            Me.Lista.Refresh()

            status = Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(0).Value.ToString + " " + "Está Online"
            If Config.notificacao = 1 Then
                Alerta(status)
            End If
            Log.status = "Está Online"
            NewLog("Sem erros")

            conn.Close()


        Catch ex As MySqlException

            status = Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(0).Value.ToString + " " + "Está Offline"
            If Config.notificacao = 1 Then
                Alerta(status)
            End If
            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Value = "Offline"
            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Style.ForeColor = Color.Red
            Me.Lista.Refresh()
            Log.status = "Está Offline"
            NewLog(ex.Message)
            conn.Close()

        End Try

    End Sub

    'Private Sub Test()
    '    Dim conn As New MySqlConnection
    '    Dim status As String

    '    Progresso.Value = 1
    '    Progresso.Maximum = _Unidades.Count

    '    For Each Me.Unidades In _Unidades
    '        conn.ConnectionString = String.Format("server={0}; user={1}; port={2} ; password={3}", Unidades.Link, Unidades.User, Unidades.Porta, Conexao.Cript(Unidades.Pass))
    '        Dim Sql = conn.CreateCommand()
    '        Sql.CommandText = "flush hosts;"
    '        Try
    '            conn.Open()
    '            Sql.ExecuteNonQuery()
    '            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Value = "Online"
    '            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Style.ForeColor = Color.Green

    '            Me.Lista.Refresh()

    '            status = Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(0).Value.ToString + " " + "Está Online"
    '            If Config.notificacao = 1 Then
    '                Alerta(status)
    '            End If
    '            Log.status = "Está Online"
    '            NewLog()

    '            conn.Close()


    '        Catch ex As MySqlException

    '            status = Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(0).Value.ToString + " " + "Está Offline"
    '            If Config.notificacao = 1 Then
    '                Alerta(status)
    '            End If
    '            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Value = "Offline"
    '            Me.Lista.Rows(_Unidades.IndexOf(Unidades)).Cells(1).Style.ForeColor = Color.Red
    '            Me.Lista.Refresh()
    '            Log.status = "Está Offline"
    '            NewLog()
    '            conn.Close()

    '        End Try
    '        Progresso.Increment(1)
    '        Thread.Sleep(500)
    '    Next

    '    Progresso.Value = 0
    'End Sub


#Region "Funções"


    Public Sub Alerta(status As String)
        Dim Trd As New Thread(
        Sub()
            Notificacao.Visible = True

            Notificacao.BalloonTipTitle = "MySQL Teste"
            Notificacao.BalloonTipText = "Unidade: " & status & ""
            Notificacao.ShowBalloonTip(1000)

        End Sub)
        Trd.Start()

    End Sub


    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Temporizador.Tick
        min = min - 1
        LblTime.Text = min.ToString + " Minuto(s)"
        If min = 0 Then

            ' Me.WindowState = FormWindowState.Normal
            'ShowInTaskbar = True
            BtnTestar.PerformClick()
            'Me.Lista.Refresh()
            ' ShowInTaskbar = False
            ' Me.WindowState = FormWindowState.Minimized

        End If

    End Sub

    Private Sub BtnFechar_Click(sender As Object, e As EventArgs) Handles BtnFechar.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
#End Region

    Private Sub AbirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbirToolStripMenuItem.Click
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub SairToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SairToolStripMenuItem.Click
        Dim box = MessageBox.Show("Deseja realmente sair ?", "Atenção!", MessageBoxButtons.YesNo)
        If box = Windows.Forms.DialogResult.Yes Then
            Me.Close()
        End If


    End Sub

    Private Sub CadastrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CadastrarToolStripMenuItem.Click
        If FrmUnidades.Visible = True Then
            FrmUnidades.Select()
        Else
            FrmUnidades.ShowDialog()
        End If

        _Unidades = Unidades.getUnidades
        Lista.DataSource = _Unidades
        Lista.Refresh()
    End Sub

    Private Sub ConfigToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ConfigToolStripMenuItem1.Click
        If FrmConfig.Visible = True Then
            FrmConfig.Select()
        Else
            FrmConfig.ShowDialog()
        End If

        If Not Config Is Nothing Then
            Config = Config.getConfig
            min = Config.tempo
        End If
        LblTime.Text = min.ToString + " Minuto(s)"
    End Sub

    Private Sub LogsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogsToolStripMenuItem.Click

        If FrmLog.Visible = True Then
            FrmLog.Select()
        Else
            FrmLog.ShowDialog()
        End If

    End Sub

    Private Sub FrmPrincipal_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\MySQL Teste", "MySQL Pid", "")
        Try
            Dim aProcess = System.Diagnostics.Process.GetProcessById(pid)
            aProcess.Kill()
        Catch
            'MessageBox.Show("Nenhum processo do mysql foi encontrado.")
        End Try


    End Sub



    Private Sub Lista_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles Lista.CellDoubleClick
        Unidades = Me.Lista.Rows(e.RowIndex).DataBoundItem
        'Dim conn As New MySqlConnection
        ' Dim status As String

        If Me.Lista.SelectedCells.Count > 0 Then
            LblTestando.Text = "Testando 1 unidade..."
            MySQL_Teste(Unidades)


            'conn.ConnectionString = String.Format("server={0}; user={1}; port={2} ; password={3}", Unidades.Link, Unidades.User, Unidades.Porta, Conexao.Cript(Unidades.Pass))
            'Dim Sql = conn.CreateCommand()
            'Sql.CommandTimeout = 3000
            'Sql.CommandText = "flush hosts;"
            'Try
            '    conn.Open()
            '    Sql.ExecuteNonQuery()
            '    Me.Lista.Rows(e.RowIndex).Cells(1).Value = "Online"
            '    Me.Lista.Rows(e.RowIndex).Cells(1).Style.ForeColor = Color.Green

            '    Me.Lista.Refresh()

            '    status = Me.Lista.Rows(e.RowIndex).Cells(0).Value.ToString + " " + "Está Online"
            '    If Config.notificacao = 1 Then
            '        Alerta(status)
            '    End If
            '    Log.status = "Está Online"

            '    NewLog()

            '    conn.Close()


            'Catch ex As MySqlException

            '    status = Me.Lista.Rows(e.RowIndex).Cells(0).Value.ToString + " " + "Está Offline"
            '    If Config.notificacao = 1 Then
            '        Alerta(status)
            '    End If
            '    Me.Lista.Rows(e.RowIndex).Cells(1).Value = "Offline"
            '    Me.Lista.Rows(e.RowIndex).Cells(1).Style.ForeColor = Color.Red
            '    Me.Lista.Refresh()
            '    Log.status = "Está Offline"
            '    NewLog()
            '    conn.Close()

            'End Try
        End If
        LblTestando.Text = "Teste Concluido."
    End Sub


    Private Sub BtnTestar_Click(sender As Object, e As EventArgs) Handles BtnTestar.Click
        Temporizador.Stop()
        LblTestando.Text = "Testando..."
        LblTestando.Refresh()
        LblTime.Text = "0 Minuto(s)"
        LblTime.Refresh()

        If _Unidades.Count = 0 Then
            MessageBox.Show("Configure uma unidade")
        Else

            For Each Rowns As DataGridViewRow In Lista.Rows
                Rowns.Cells(1).Value = String.Empty
                Me.Lista.Refresh()
            Next
            '' Test()


            ProgressBarControlTeste.Properties.Maximum = _Unidades.Count
            ProgressBarControlTeste.Properties.Minimum = 0
            ProgressBarControlTeste.Properties.PercentView = True
            ProgressBarControlTeste.Properties.Step = 1



            For Each Me.Unidades In _Unidades
                MySQL_Teste(Unidades)
                ProgressBarControlTeste.PerformStep()
                ProgressBarControlTeste.Update()
                Thread.Sleep(500)
            Next


            ' ProgressBarControlTeste.Reset()
            If Config Is Nothing Then
                min = 30
            Else
                min = Config.tempo
            End If
            LblTime.Text = min.ToString + " Minuto(s)"

            LblTestando.Text = "Teste Concluido."
            Temporizador.Start()

        End If

        ProgressBarControlTeste.Reset()
        ProgressBarControlTeste.EditValue = 0
    End Sub


End Class
