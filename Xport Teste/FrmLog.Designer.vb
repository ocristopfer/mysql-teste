﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DtpInicial = New System.Windows.Forms.DateTimePicker()
        Me.DtpFinal = New System.Windows.Forms.DateTimePicker()
        Me.BtnFiltrar = New System.Windows.Forms.Button()
        Me.CmboxUnidades = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LblUnidades = New System.Windows.Forms.Label()
        Me.BtnExecel = New System.Windows.Forms.Button()
        Me.DgvLog = New DevExpress.XtraGrid.GridControl()
        Me.DgvLogs = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Unidade = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Data = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Status = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LblOnline = New System.Windows.Forms.Label()
        Me.LblOffline = New System.Windows.Forms.Label()
        Me.ListUnidades = New System.Windows.Forms.ListBox()
        Me.CheckBoxAtivos = New System.Windows.Forms.CheckBox()
        Me.Erro = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.DgvLog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DtpInicial
        '
        Me.DtpInicial.Checked = False
        Me.DtpInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpInicial.Location = New System.Drawing.Point(73, 9)
        Me.DtpInicial.Name = "DtpInicial"
        Me.DtpInicial.ShowCheckBox = True
        Me.DtpInicial.Size = New System.Drawing.Size(128, 20)
        Me.DtpInicial.TabIndex = 1
        '
        'DtpFinal
        '
        Me.DtpFinal.Checked = False
        Me.DtpFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpFinal.Location = New System.Drawing.Point(73, 37)
        Me.DtpFinal.Name = "DtpFinal"
        Me.DtpFinal.ShowCheckBox = True
        Me.DtpFinal.Size = New System.Drawing.Size(128, 20)
        Me.DtpFinal.TabIndex = 2
        '
        'BtnFiltrar
        '
        Me.BtnFiltrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnFiltrar.Location = New System.Drawing.Point(443, 47)
        Me.BtnFiltrar.Name = "BtnFiltrar"
        Me.BtnFiltrar.Size = New System.Drawing.Size(108, 23)
        Me.BtnFiltrar.TabIndex = 3
        Me.BtnFiltrar.Text = "Filtrar"
        Me.BtnFiltrar.UseVisualStyleBackColor = True
        '
        'CmboxUnidades
        '
        Me.CmboxUnidades.FormattingEnabled = True
        Me.CmboxUnidades.Location = New System.Drawing.Point(220, 42)
        Me.CmboxUnidades.Name = "CmboxUnidades"
        Me.CmboxUnidades.Size = New System.Drawing.Size(121, 21)
        Me.CmboxUnidades.TabIndex = 4
        Me.CmboxUnidades.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Data Inicial:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Data Final:"
        '
        'LblUnidades
        '
        Me.LblUnidades.AutoSize = True
        Me.LblUnidades.Location = New System.Drawing.Point(557, 84)
        Me.LblUnidades.Name = "LblUnidades"
        Me.LblUnidades.Size = New System.Drawing.Size(55, 13)
        Me.LblUnidades.TabIndex = 7
        Me.LblUnidades.Text = "Unidades:"
        '
        'BtnExecel
        '
        Me.BtnExecel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnExecel.Location = New System.Drawing.Point(413, 361)
        Me.BtnExecel.Name = "BtnExecel"
        Me.BtnExecel.Size = New System.Drawing.Size(138, 23)
        Me.BtnExecel.TabIndex = 8
        Me.BtnExecel.Text = "Exportar para Excel"
        Me.BtnExecel.UseVisualStyleBackColor = True
        '
        'DgvLog
        '
        Me.DgvLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.DgvLog.Location = New System.Drawing.Point(12, 83)
        Me.DgvLog.MainView = Me.DgvLogs
        Me.DgvLog.Name = "DgvLog"
        Me.DgvLog.Size = New System.Drawing.Size(539, 268)
        Me.DgvLog.TabIndex = 9
        Me.DgvLog.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.DgvLogs})
        '
        'DgvLogs
        '
        Me.DgvLogs.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Unidade, Me.Data, Me.Status, Me.Erro})
        Me.DgvLogs.GridControl = Me.DgvLog
        Me.DgvLogs.Name = "DgvLogs"
        Me.DgvLogs.OptionsView.ShowGroupPanel = False
        '
        'Unidade
        '
        Me.Unidade.Caption = "Unidade"
        Me.Unidade.FieldName = "unidade"
        Me.Unidade.Name = "Unidade"
        Me.Unidade.Visible = True
        Me.Unidade.VisibleIndex = 0
        '
        'Data
        '
        Me.Data.Caption = "Data"
        Me.Data.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Data.FieldName = "dateteste"
        Me.Data.Name = "Data"
        Me.Data.Visible = True
        Me.Data.VisibleIndex = 1
        '
        'Status
        '
        Me.Status.Caption = "Status"
        Me.Status.FieldName = "status"
        Me.Status.Name = "Status"
        Me.Status.Visible = True
        Me.Status.VisibleIndex = 2
        '
        'LblOnline
        '
        Me.LblOnline.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblOnline.AutoSize = True
        Me.LblOnline.Location = New System.Drawing.Point(612, 27)
        Me.LblOnline.Name = "LblOnline"
        Me.LblOnline.Size = New System.Drawing.Size(67, 13)
        Me.LblOnline.TabIndex = 10
        Me.LblOnline.Text = "Total Online:"
        '
        'LblOffline
        '
        Me.LblOffline.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblOffline.AutoSize = True
        Me.LblOffline.Location = New System.Drawing.Point(612, 52)
        Me.LblOffline.Name = "LblOffline"
        Me.LblOffline.Size = New System.Drawing.Size(67, 13)
        Me.LblOffline.TabIndex = 11
        Me.LblOffline.Text = "Total Offline:"
        '
        'ListUnidades
        '
        Me.ListUnidades.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListUnidades.FormattingEnabled = True
        Me.ListUnidades.Location = New System.Drawing.Point(557, 100)
        Me.ListUnidades.Name = "ListUnidades"
        Me.ListUnidades.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ListUnidades.Size = New System.Drawing.Size(145, 251)
        Me.ListUnidades.TabIndex = 12
        '
        'CheckBoxAtivos
        '
        Me.CheckBoxAtivos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxAtivos.AutoSize = True
        Me.CheckBoxAtivos.Checked = True
        Me.CheckBoxAtivos.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxAtivos.Location = New System.Drawing.Point(644, 80)
        Me.CheckBoxAtivos.Name = "CheckBoxAtivos"
        Me.CheckBoxAtivos.Size = New System.Drawing.Size(55, 17)
        Me.CheckBoxAtivos.TabIndex = 13
        Me.CheckBoxAtivos.Text = "Ativos"
        Me.CheckBoxAtivos.UseVisualStyleBackColor = True
        '
        'Erro
        '
        Me.Erro.Caption = "Erro"
        Me.Erro.FieldName = "erro"
        Me.Erro.Name = "Erro"
        Me.Erro.Visible = True
        Me.Erro.VisibleIndex = 3
        '
        'FrmLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 396)
        Me.Controls.Add(Me.CheckBoxAtivos)
        Me.Controls.Add(Me.ListUnidades)
        Me.Controls.Add(Me.LblOffline)
        Me.Controls.Add(Me.LblOnline)
        Me.Controls.Add(Me.DgvLog)
        Me.Controls.Add(Me.BtnExecel)
        Me.Controls.Add(Me.LblUnidades)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CmboxUnidades)
        Me.Controls.Add(Me.BtnFiltrar)
        Me.Controls.Add(Me.DtpFinal)
        Me.Controls.Add(Me.DtpInicial)
        Me.Name = "FrmLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Log de Testes"
        CType(Me.DgvLog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DtpInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents DtpFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnFiltrar As System.Windows.Forms.Button
    Friend WithEvents CmboxUnidades As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LblUnidades As System.Windows.Forms.Label
    Friend WithEvents BtnExecel As System.Windows.Forms.Button
    Friend WithEvents DgvLog As DevExpress.XtraGrid.GridControl
    Friend WithEvents DgvLogs As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Unidade As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Data As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Status As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LblOnline As System.Windows.Forms.Label
    Friend WithEvents LblOffline As System.Windows.Forms.Label
    Friend WithEvents ListUnidades As System.Windows.Forms.ListBox
    Friend WithEvents CheckBoxAtivos As System.Windows.Forms.CheckBox
    Friend WithEvents Erro As DevExpress.XtraGrid.Columns.GridColumn
End Class
