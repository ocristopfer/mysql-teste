﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' As informações gerais sobre um assembly são controladas por
' conjunto de atributos. Altere estes valores de atributo para modificar as informações
' associada a um assembly.

' Revise os valores dos atributos do assembly

<Assembly: AssemblyTitle("MySQL Teste")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Cristopfer Luis")> 
<Assembly: AssemblyProduct("MySQL Teste")> 
<Assembly: AssemblyCopyright("Copyright ©  2017-2018")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'O GUID a seguir será destinado à ID de typelib se este projeto for exposto para COM
<Assembly: Guid("9322eb86-40c1-4d37-81ac-34991fc8d848")> 

' As informações da versão de um assembly consistem nos quatro valores a seguir:
'
'      Versão Principal
'      Versão Secundária 
'      Número da Versão
'      Revisão
'
' É possível especificar todos os valores ou usar como padrão os Números da Versão e da Revisão 
' utilizando o "*" como mostrado abaixo:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.8.3.0")> 
<Assembly: AssemblyFileVersion("1.8.3.0")> 
