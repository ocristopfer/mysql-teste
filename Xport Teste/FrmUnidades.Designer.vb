﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUnidades
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DgvUnidades = New System.Windows.Forms.DataGridView()
        Me.colunUnidades = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.collunLink = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.PnDados = New System.Windows.Forms.Panel()
        Me.CboxAtivo = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPorta = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLink = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtUnidade = New System.Windows.Forms.TextBox()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.BtnExcluir = New System.Windows.Forms.Button()
        CType(Me.DgvUnidades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnDados.SuspendLayout()
        Me.SuspendLayout()
        '
        'DgvUnidades
        '
        Me.DgvUnidades.AllowUserToAddRows = False
        Me.DgvUnidades.AllowUserToDeleteRows = False
        Me.DgvUnidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvUnidades.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colunUnidades, Me.collunLink})
        Me.DgvUnidades.Location = New System.Drawing.Point(12, 12)
        Me.DgvUnidades.Name = "DgvUnidades"
        Me.DgvUnidades.ReadOnly = True
        Me.DgvUnidades.RowHeadersVisible = False
        Me.DgvUnidades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvUnidades.Size = New System.Drawing.Size(358, 340)
        Me.DgvUnidades.TabIndex = 0
        '
        'colunUnidades
        '
        Me.colunUnidades.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colunUnidades.DataPropertyName = "Unidade"
        Me.colunUnidades.HeaderText = "Unidades"
        Me.colunUnidades.Name = "colunUnidades"
        Me.colunUnidades.ReadOnly = True
        '
        'collunLink
        '
        Me.collunLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.collunLink.DataPropertyName = "Link"
        Me.collunLink.HeaderText = "Link"
        Me.collunLink.Name = "collunLink"
        Me.collunLink.ReadOnly = True
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(387, 209)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(75, 23)
        Me.btnNovo.TabIndex = 19
        Me.btnNovo.Text = "Incluir"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Enabled = False
        Me.btnCancelar.Location = New System.Drawing.Point(664, 209)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 21
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'PnDados
        '
        Me.PnDados.BackColor = System.Drawing.Color.White
        Me.PnDados.Controls.Add(Me.CboxAtivo)
        Me.PnDados.Controls.Add(Me.Label5)
        Me.PnDados.Controls.Add(Me.txtPorta)
        Me.PnDados.Controls.Add(Me.Label4)
        Me.PnDados.Controls.Add(Me.txtPass)
        Me.PnDados.Controls.Add(Me.Label3)
        Me.PnDados.Controls.Add(Me.txtUser)
        Me.PnDados.Controls.Add(Me.Label2)
        Me.PnDados.Controls.Add(Me.txtLink)
        Me.PnDados.Controls.Add(Me.Label1)
        Me.PnDados.Controls.Add(Me.txtUnidade)
        Me.PnDados.Enabled = False
        Me.PnDados.Location = New System.Drawing.Point(376, 12)
        Me.PnDados.Name = "PnDados"
        Me.PnDados.Size = New System.Drawing.Size(363, 171)
        Me.PnDados.TabIndex = 22
        '
        'CboxAtivo
        '
        Me.CboxAtivo.AutoSize = True
        Me.CboxAtivo.Location = New System.Drawing.Point(62, 140)
        Me.CboxAtivo.Name = "CboxAtivo"
        Me.CboxAtivo.Size = New System.Drawing.Size(50, 17)
        Me.CboxAtivo.TabIndex = 29
        Me.CboxAtivo.Text = "Ativo"
        Me.CboxAtivo.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Porta:"
        '
        'txtPorta
        '
        Me.txtPorta.Location = New System.Drawing.Point(63, 114)
        Me.txtPorta.Name = "txtPorta"
        Me.txtPorta.Size = New System.Drawing.Size(72, 20)
        Me.txtPorta.TabIndex = 27
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Senha:"
        '
        'txtPass
        '
        Me.txtPass.Location = New System.Drawing.Point(63, 88)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(296, 20)
        Me.txtPass.TabIndex = 25
        Me.txtPass.UseSystemPasswordChar = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Usuário:"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(62, 62)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(296, 20)
        Me.txtUser.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Link:"
        '
        'txtLink
        '
        Me.txtLink.Location = New System.Drawing.Point(62, 36)
        Me.txtLink.Name = "txtLink"
        Me.txtLink.Size = New System.Drawing.Size(296, 20)
        Me.txtLink.TabIndex = 21
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Unidade:"
        '
        'txtUnidade
        '
        Me.txtUnidade.Location = New System.Drawing.Point(62, 10)
        Me.txtUnidade.Name = "txtUnidade"
        Me.txtUnidade.Size = New System.Drawing.Size(296, 20)
        Me.txtUnidade.TabIndex = 19
        '
        'btnSalvar
        '
        Me.btnSalvar.Enabled = False
        Me.btnSalvar.Location = New System.Drawing.Point(574, 209)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(84, 23)
        Me.btnSalvar.TabIndex = 23
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'BtnExcluir
        '
        Me.BtnExcluir.Enabled = False
        Me.BtnExcluir.Location = New System.Drawing.Point(468, 209)
        Me.BtnExcluir.Name = "BtnExcluir"
        Me.BtnExcluir.Size = New System.Drawing.Size(75, 23)
        Me.BtnExcluir.TabIndex = 24
        Me.BtnExcluir.Text = "Excluir"
        Me.BtnExcluir.UseVisualStyleBackColor = True
        '
        'FrmUnidades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(752, 362)
        Me.Controls.Add(Me.BtnExcluir)
        Me.Controls.Add(Me.btnSalvar)
        Me.Controls.Add(Me.PnDados)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNovo)
        Me.Controls.Add(Me.DgvUnidades)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "FrmUnidades"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cadastrar Unidades"
        CType(Me.DgvUnidades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnDados.ResumeLayout(False)
        Me.PnDados.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DgvUnidades As System.Windows.Forms.DataGridView
    Friend WithEvents btnNovo As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents PnDados As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPorta As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtLink As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUnidade As System.Windows.Forms.TextBox
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents colunUnidades As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents collunLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CboxAtivo As System.Windows.Forms.CheckBox
    Friend WithEvents BtnExcluir As System.Windows.Forms.Button
End Class
